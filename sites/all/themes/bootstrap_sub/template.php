<?php

/**
 * @file
 * template.php
 */


function bootstrap_sub_preprocess_html(&$variables) {
 // 	drupal_add_css(drupal_get_path('theme', 'omage_sub') . '/js/icheck/skins/line/custom.css', array(
	// 	'group' => CSS_THEME, 
	// 	'type' => 'file'
	// ));
	  //echo 'aaa';
	// drupal_add_js(drupal_get_path('theme', 'omage_sub') . '/js/icheck/icheck.js', array( 
	//   'scope' => 'header', 
	//   'weight' => '14' 
	// ));
	drupal_add_js(drupal_get_path('theme', 'bootstrap_sub') . '/js/custom.js', array( 
	  'scope' => 'footer', 
	  'weight' => '15' 
	));
	

	
	
}

function bootstrap_sub_views_pre_render(&$view) {
	
	
  if ($view->name == 'filter_product_reference' && ( $view->current_display == 'page' ) && $view->exposed_raw_input['field_visible_transmission_value']['max'] == '') {
	  
	  
	  $view->result = null;
	  $view->empty  = '';
	}
	if($view->name == 'wers_performance_data'){
		if(!isset($_GET['field_company_tid']) || $_GET['field_company_tid'] == 'All' ){
			
	    	  $view->result = null;
	    	  $view->empty  = '';
		}
  	
		
	}
	
}


 function bootstrap_sub_form_alter(&$form, &$form_state, $form_id) {
	
	if($form_id == 'views_exposed_form')   {
		
		$form['submit']['#value'] = 'FIND PRODUCTS';
		$form['submit']['#suffix'] = '<div class="text_suffix">Results that match your search:</div>';
	}
}

function bootstrap_sub_preprocess_page(&$vars, $hook) {
  if (isset($vars['node']->type)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
}
