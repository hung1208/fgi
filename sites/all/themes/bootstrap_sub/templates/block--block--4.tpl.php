
<?php
/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see bootstrap_preprocess_block()
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see bootstrap_process_block()
 * @see template_process()
 *
 * @ingroup templates
 */
?>

<section id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div id="myCarousel" class="carousel slide carousel-inner carousel-fade" data-ride="carousel">
	    
	    <?php 
		        $count = 0;
				$type = "slide"; 
				$nodes = node_load_multiple(array(), array('type' => $type)); 
				
				
				
				foreach($nodes as $products):
				$image = file_create_url($products->field_image['und'][0]['uri']);
				$body = '';
				if(count($products->body)>0){
					$body = $products->body['und'][0]['value'];	
				}
				
				?>
				<div class="item <?php if($count == 0){?> active <?php }?>"> <img src="<?php echo $image?>" alt="First slide">
					<div class="body-mobile">
						<?php echo $body?>
						
					</div>
				</div>
				
				<?php $count ++; endforeach; ?>
				
		<ol class="carousel-indicators">
			<?php for ($i = 0; $i <= $count-1; $i++) {?>
			    <li data-target="#myCarousel" data-slide-to="<?php echo $i?>" <?php if($i==0){?>class="active"<?php }?>></li>
			<?php }?>
			
	    </ol>
	    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left">&nbsp;</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right">&nbsp;</span>
            </a>	</div>	
	
	
<?php if (strpos($classes, 'have-container') !== false) {?>
	<div class="container">
<?php }?>
	<div class="top-slide">
	  <?php //print $content ?>
	</div>
<?php if (strpos($classes, 'have-container') !== false) {?>
	</div>
<?php }?>
</section>
