<?php drupal_add_js(drupal_get_path('theme', 'bootstrap_sub').'/js/lib/hash.js');
		//drupal_add_js(drupal_get_path('theme', 'bootstrap_sub').'/js/lib/turn.js');
		drupal_add_js(drupal_get_path('theme', 'bootstrap_sub').'/js/extras/modernizr.2.5.3.min.js');
		$path = drupal_get_path('theme', 'bootstrap_sub').'/js/';
		?>
<div class="wrapper-catalog">
	<div class="flipbook-viewport">
		<div class="container">
			<div class="flipbook">
				<?php foreach($node->field_brochure['und'] as $row){
					$img = image_style_url('brochure',$row['uri']);
					
				?> 	
					<div class="catalog-page">
						<img src="<?php echo $img ?>" />	
					</div>
					
				<?php }?>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">

function loadApp() {

	// Create the flipbook
	
	Hash.on('^page\/([0-9]*)$', {
		yep: function(path, parts) {
			var page = parts[1];

			if (page!==undefined) {
				if ((jQuery)('.flipbook').turn('is'))
					(jQuery)('.flipbook').turn('page', page);
			}

		},
		nop: function(path) {

			if ((jQuery)('.flipbook').turn('is'))
				(jQuery)('.flipbook').turn('page', 1);
		}
	});
	
	(jQuery)('.flipbook').turn({
			// Width

			width:1400,
			
			// Height

			height:500,

			// Elevation

			elevation: 50,
			
			// Enable gradients

			gradients: true,
			
			// Auto center this flipbook

			autoCenter: true,
			
			when: {
			   turning: function(event, page, view) {
			
			
			   (jQuery)(this).turn('page');
			   Hash.go('page/' + page).update();
			   //Hash.go('page/' + page).update();
				}
			}
			

	});
}

// Load the HTML4 version if there's not CSS transform

yepnope({
	test : Modernizr.csstransforms,
	yep: ['<?php echo $path?>/lib/turn.js'],
	nope: ['<?php echo $path?>/lib/turn.html4.min.js'],
	both: ['<?php echo $path?>/css/basic.css'],
	complete: loadApp
});

</script>