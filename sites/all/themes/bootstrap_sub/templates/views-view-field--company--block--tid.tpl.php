<?php $tid = $output?>

<?php $term = taxonomy_term_load($tid);
$image =  file_create_url($term->field_company_image['und'][0]['uri']);
$link = $term->field_link['und'][0]['url'];
?>

<div class="col-xs-12 col-sm-6 col-md-6 taxo_image">
	<a href="<?php echo $link?>"><img src="<?php echo $image?>"></a>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 taxo_link">
	<a href="<?php echo $link?>" target="_blank" class="new-button">VISIT WEBSITE</a>
</div>
