
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  

  <div class="content"<?php print $content_attributes; ?>>
	 <div class="title"><?php print render($content['field_glass_makeup']); ?></div>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>


</div>
