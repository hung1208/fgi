function setClipboard(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}
var check = true;
(function($){
	$('.button-glass').click(function(){
		var target = $(this).attr('target');
		$('.button-glass').removeClass('active');
		$(this).addClass('active');
		if(target == 7){
			$('.image_taxo').removeClass('show');
			$('#image_single').addClass('show');
		}else{
			$('.image_taxo').removeClass('show');
			$('#image_double').addClass('show');
		}
		
		
		$('#edit-field-glass-type-tid .form-radio').each(function(){
			if($(this).val() == target){
				$(this).parent().trigger('click');
			}
			
		});
	});
	$('.close-toggle').click(function(){
		$('.toggle-slide').hide();
		
	});
	
	$('#edit-field-visible-transmission-value-wrapper .form-item-field-visible-transmission-value-min label').html('%');
	$('#edit-field-visible-transmission-value-wrapper .form-item-field-visible-transmission-value-max label').html('%');
	
	$('#edit-field-u-value-value-wrapper .form-item-field-u-value-value-min label').html('W/m2K');
	$('#edit-field-u-value-value-wrapper .form-item-field-u-value-value-max label').html('W/m2K');
	
	$('#edit-field-solar-heat-gain-coefficien-value-wrapper .form-item-field-solar-heat-gain-coefficien-value-min label').html('');
	$('#edit-field-solar-heat-gain-coefficien-value-wrapper .form-item-field-solar-heat-gain-coefficien-value-max label').html('-');
	
	$('.popup-control').click(function(){
		$('#overlay-login').show();
		$('.wrapper-popup-glass').show();
		$('.wrapper-popup-glass').focus();
		var choose = $(this).attr('target');
		$('#content-popup .popup').hide();
		$(choose).show();
		$("html, body").animate({ scrollTop: 200 }, 1500);
		
		
	});
	
	$('#overlay-login').click(function(){
		$('#overlay-login').hide();
		$('.wrapper-popup-glass').hide();
		
	});
	$('.close-popup').click(function(){
		$('#overlay-login').hide();
		$('.wrapper-popup-glass').hide();
		
	});
	$('.copyClip').click(function(){
	   var value = $('#currentUrl').val();
	   setClipboard(value);
	   alert('Copy success!');
	});
	
	$('.view-latest-projects-news .views-row-1 a').attr('href','/sites/all/themes/bootstrap_sub/EDM/fgi/juine16/FGI-june-2016.html');
	$('.view-latest-projects-news .views-row-2 a').attr('href','/sites/all/themes/bootstrap_sub/EDM/fgi/dec-2015/fgi-dec.html');
	$('.view-latest-projects-news .views-row-3 a').attr('href','/glass-range/energy/optema');
	$('.view-latest-projects-news .views-row-1 a').attr('target','_blank');
	$('.view-latest-projects-news .views-row-2 a').attr('target','_blank');
	//$('.view-latest-projects-news .views-row-3 a').attr('target','_blank');
	if( $('.front').length > 0 ) {
		$('.main-container').addClass('container-fluid');
		$('.main-container').removeClass('container');
	}
	$('#navbar').addClass('container-fluid');
	$('#navbar').removeClass('container');

	$(document).ready(function(){
		if( $('.page-taxonomy').length > 0 ) {
			$('.term-listing-heading').appendTo( $('#page-header') );
		}

		if( $('.node-type-products').length > 0 ) {
			//Banner
			$('.field-name-field-product-image-banner').appendTo( $('#page-header') );
			///$('.field-name-field-product-short-description').appendTo( $('.field-name-field-product-image-banner') );	
			$('.field-name-field-description').appendTo( $('.field-name-field-product-image-banner') );	
			//Content
			$('.field-name-body').appendTo( $('.tab-content #comemrcial') );
			$('.field-name-field-product-residential').appendTo( $('.tab-content #residential') );
			var count = $("#residential").html().length;
			if(count==0){
				$("#residential_li").hide();
			}
			var count = $("#comemrcial").html().length;
			if(count==0){
				$("#COMMERCIAL_li").hide();
			}
			
		}
		
		//alert('dsadsa');
		$('#goto').change(function(){
			var goto = $(this).val();
			window.location = '/'+goto;
			
		});
		$('#changeiframe').change(function(){
			var goto = $(this).val();
			$('#chart').attr('src',goto);
			
		});
		$.fn.hasAttr = function(name) {  
		   return this.attr(name) !== undefined;
		};
		$('#block-menu-menu-glass-range .dropdown-toggle').attr('first','no');
		$('#block-menu-menu-glass-range .dropdown-toggle').click(function(){
			
			var attr = $(this).attr('first');
			//alert(attr);
			if(attr=='no') {
			   $('#block-menu-menu-glass-range .dropdown-menu').hide();
			   $(this).attr('first','yes');
			   $(this).parent().children('.dropdown-menu').slideToggle("slow");
			}else{
				window.location = $(this).attr('href');
			}
			return false;
		});
		$('#block-menu-menu-glass-range .dropdown-toggle').each(function(){
			var href = $(this).attr('href');
			var pathname = window.location.pathname; 
			if(pathname == href){
				$(this).parent().children('.dropdown-menu').show();
			}
		});
		
		$('#block-menu-menu-glass-range--2 .dropdown-toggle').attr('first','no');
		$('#block-menu-menu-glass-range--2 .dropdown-toggle').click(function(){
			
			var attr = $(this).attr('first');
			//alert(attr);
			if(attr=='no') {
			   $('#block-menu-menu-glass-range--2 .dropdown-menu').hide();
			   $(this).attr('first','yes');
			   $(this).parent().children('.dropdown-menu').slideToggle("slow");
			}else{
				window.location = $(this).attr('href');
			}
			return false;
		});
		$('#block-menu-menu-glass-range--2 .dropdown-toggle').each(function(){
			var href = $(this).attr('href');
			var pathname = window.location.pathname; 
			if(pathname == href){
				$(this).parent().children('.dropdown-menu').show();
			}
		});
		
		
		$("#menu_custom_new").click(function(){
			if($(this).hasClass('close')){
				$(this).removeClass('close');
			}else{
				$(this).addClass('close');
			}
			$('#block-menu-menu-glass-range .menu').slideToggle("slow");
			
		});
		
		$('.view-resource-center-accordion span.file a').attr('target', '_blank');
	});
})(jQuery);