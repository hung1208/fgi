<?php
$image = $row->field_field_image[0]['raw']['uri'];

$body = drupal_render($data->field_body);
?>

<div class='slide-front' style='background: url(<?php echo file_create_url($image)?>) no-repeat'>
	<div class='slide-front-title'> <?php echo $row->node_title?></div>
	<div class='slide-front-body'><?php echo $body?></div>
</div>