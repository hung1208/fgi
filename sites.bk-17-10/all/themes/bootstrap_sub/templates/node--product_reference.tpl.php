<?php $node = node_load($node->nid);
$product = node_load($node->field_product['und'][0]['nid']);

$image_product = $product->field_product_image['und'][0]['uri'];
$title_product = $product->title;
$des_product = $product->field_description['und'][0]['value'];
?>
<div class="wrapper-node-refer node-<?php echo $product->nid?>">
		<div class="col-xs-12 col-md-3">
			<img src="<?php echo image_style_url('product_image_refer',$image_product)?>">
		</div>

		<div class="col-xs-12 col-md-9">
			<h3><?php echo $title_product;?></h3>
			<?php echo $des_product;?>
		</div>
</div>

<div class="table-reponsitive">

