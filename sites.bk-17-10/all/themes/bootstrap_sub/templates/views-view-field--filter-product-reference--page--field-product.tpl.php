<?php


$node_refer_nid = $row->field_field_product['0']['raw']['nid'];


$product = node_load($node_refer_nid);

?>
<?php 
$image_product = '/sites/default/files/default_images/bg-default.png';
if(count($product->field_product_image)>0){
		$image_product = $product->field_product_image['und'][0]['uri'];
}

$title_product = $product->title;
$des_product = $product->field_description['und'][0]['value'];
?>
<div class="wrapper-node-refer node-<?php echo $product->nid?>">
		<div class="col-xs-12 col-sm-4 col-md-3">
			
			<?php if(count($product->field_product_image)>0){?>
				<img src="<?php echo image_style_url('product_image_refer',$image_product)?>">
				<?php }else{?>
			<img src="<?php echo $image_product?>">
			<?php }?>
			
		</div>

		<div class="col-xs-12 col-sm-8 col-md-9">
			<h3 class="title_glass"><?php echo $title_product;?></h3>
			<?php echo $des_product;?>
			<?php if(count($product->field_dont_show_view_product) > 0){
				if($product->field_dont_show_view_product['und'][0]['value'] == 1){
					
				}else{?>
					<a href="<?php echo url('node/'.$node_refer_nid)?>" target="_blank" class="btn btn-glass">VIEW PRODUCT</a>
				<?php }
			}else{?>
			<a href="<?php echo url('node/'.$node_refer_nid)?>" target="_blank" class="btn btn-glass">VIEW PRODUCT</a>	
			<?php }?>
		</div>
</div>



