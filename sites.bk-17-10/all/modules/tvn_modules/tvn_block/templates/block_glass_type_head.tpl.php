<div class="col-xs-12 col-md-12 top-page">
	<?php 
	$tid = 8;
	if(isset($_GET['field_glass_type_tid'])){
		if($_GET['field_glass_type_tid'] != 'All'){
				$tid = $_GET['field_glass_type_tid'];
		}
		
	}
	?>
	<?php print views_embed_view('taxonomy_term_body', 'block', $tid);?>
</div>
<div class="col-xs-12 col-md-12">
	<?php
		$block = block_load('block', '39');
		$blockrender = _block_render_blocks(array($block));
		$render_aray  = _block_get_renderable_array($blockrender);
		$output = drupal_render($render_aray);
		print $output;
		?>
</div>

<div class="col-xs-12 col-md-12" id ="block-views-taxonomy-term-body-block-1">
	
	
	<?php if(!isset($_GET['field_glass_type_tid'])){?>
		<div class="toggle-slide">
			<div class="close-toggle">x</div>
			<div class="toggle-slide-content">Toggle sliders to your requirements</div>
		</div>
		<div class="image">
			<img class="img-responsive image_taxo show" id="image_double" typeof="foaf:Image" src="/sites/default/files/image1.png" width="215" height="400" />
			<img class="img-responsive image_taxo hide" id="image_single" typeof="foaf:Image" src="/sites/default/files/image2.png" width="215" height="400" />
		</div>
		
	<?php }else{?>
		
			<div class="image">
				<img class="img-responsive image_taxo <?php if($_GET['field_glass_type_tid'] == 8){?> show <?php }?>" id="image_double" typeof="foaf:Image" src="/sites/default/files/image1.png" width="215" height="400" />
				<img class="img-responsive image_taxo <?php if($_GET['field_glass_type_tid'] == 7){?> show <?php }?>" id="image_single" typeof="foaf:Image" src="/sites/default/files/image2.png" width="215" height="400" />
			</div>
			
	<?php }?>
	<?php //print views_embed_view('taxonomy_term_body', 'block_1', $tid);?>
	
	
	
	
	<?php
		$block = block_load('views', '7a387057ff6388c99fbda8232c9457ce');
		$blockrender = _block_render_blocks(array($block));
		$render_aray  = _block_get_renderable_array($blockrender);
		$output = drupal_render($render_aray);
		print $output;
		?>
		
	
</div>





<?php $current_url = url(current_path(), array('absolute' => TRUE,'query' => drupal_get_query_parameters()));?>
<div id="overlay-login" class=""></div>

<div class="wrapper-popup-glass">
	<div class="close-popup">x</div>
<div class="popup-glass">
	
	<div id="content-popup">
			<div class="popup" id="popup-email">
				<h3>Share via Email</h3>
				<p>Interactive Glass Performance Results<br/>
					<?php echo $current_url?></p>
				<?php echo render(drupal_get_form('form_sendemail_form')) ?>
			</div>
			
			<div class="popup" id="popup-sharelink">
				<h3>Copy Link</h3>
				<p>Interactive Glass Performance Results</p>
				<input type="text" id="currentUrl" value="<?php echo $current_url?>">
				<a href="#" class="copyClip btn btn-glass">COPY TO CLIPBOARD</a>
			</div>
			
	</div>	
</div>
</div>




