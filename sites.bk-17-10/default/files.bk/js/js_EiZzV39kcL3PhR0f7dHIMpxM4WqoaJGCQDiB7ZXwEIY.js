/**
 * @file
 * Applies responsive-nav.js settings.
 */
(function ($) {

  Drupal.responsive_navigation = Drupal.responsive_navigation || {};
  Drupal.behaviors.responsive_navigation = {
    attach: function (context, settings) {
      settings.responsive_navigation = settings.responsive_navigation || {};
      Drupal.responsive_navigation.blocks = Drupal.responsive_navigation.blocks || {};
      if (!$("#nav").size() || settings.responsive_navigation.total == 0) return; // end if no #nav is found or no blocks enabled.
      $.each(settings.responsive_navigation.blocks, function(key, value) {
        /* Method for handling multiple navs in the future */
        // key = key.split('_').join('-'); // convert _ to -
        // Drupal.responsive_navigation.blocks[key] = responsiveNav("#" + key, { // Selector: The ID of the wrapper
        Drupal.responsive_navigation.blocks['navigation'] = responsiveNav("#nav", { // Selector: The ID of the wrapper
          animate: value['responsive_navigation_animate'], // Boolean: Use CSS3 transitions, true or false
          transition: value['responsive_navigation_transition'], // Integer: Speed of the transition, in milliseconds
          label: value['responsive_navigation_label'], // String: Label for the navigation toggle
          insert: value['responsive_navigation_insert'], // String: Insert the toggle before or after the navigation
          openPos: value['responsive_navigation_openpos'], // String: Position of the opened nav, relative or static
          jsClass: value['responsive_navigation_jsclass'], // String: 'JS enabled' class which is added to <html> el
        });
      });
    },
  };

}(jQuery));
;
(function ($) {

/**
 * Retrieves the summary for the first element.
 */
$.fn.drupalGetSummary = function () {
  var callback = this.data('summaryCallback');
  return (this[0] && callback) ? $.trim(callback(this[0])) : '';
};

/**
 * Sets the summary for all matched elements.
 *
 * @param callback
 *   Either a function that will be called each time the summary is
 *   retrieved or a string (which is returned each time).
 */
$.fn.drupalSetSummary = function (callback) {
  var self = this;

  // To facilitate things, the callback should always be a function. If it's
  // not, we wrap it into an anonymous function which just returns the value.
  if (typeof callback != 'function') {
    var val = callback;
    callback = function () { return val; };
  }

  return this
    .data('summaryCallback', callback)
    // To prevent duplicate events, the handlers are first removed and then
    // (re-)added.
    .unbind('formUpdated.summary')
    .bind('formUpdated.summary', function () {
      self.trigger('summaryUpdated');
    })
    // The actual summaryUpdated handler doesn't fire when the callback is
    // changed, so we have to do this manually.
    .trigger('summaryUpdated');
};

/**
 * Sends a 'formUpdated' event each time a form element is modified.
 */
Drupal.behaviors.formUpdated = {
  attach: function (context) {
    // These events are namespaced so that we can remove them later.
    var events = 'change.formUpdated click.formUpdated blur.formUpdated keyup.formUpdated';
    $(context)
      // Since context could be an input element itself, it's added back to
      // the jQuery object and filtered again.
      .find(':input').andSelf().filter(':input')
      // To prevent duplicate events, the handlers are first removed and then
      // (re-)added.
      .unbind(events).bind(events, function () {
        $(this).trigger('formUpdated');
      });
  }
};

/**
 * Prepopulate form fields with information from the visitor cookie.
 */
Drupal.behaviors.fillUserInfoFromCookie = {
  attach: function (context, settings) {
    $('form.user-info-from-cookie').once('user-info-from-cookie', function () {
      var formContext = this;
      $.each(['name', 'mail', 'homepage'], function () {
        var $element = $('[name=' + this + ']', formContext);
        var cookie = $.cookie('Drupal.visitor.' + this);
        if ($element.length && cookie) {
          $element.val(cookie);
        }
      });
    });
  }
};

})(jQuery);
;
